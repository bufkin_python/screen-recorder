import cv2
import keyboard
import numpy
import pyautogui

filename = "record.mp4"
SCREEN_SIZE = (1920, 1080)
vid = cv2.VideoWriter(filename, cv2.VideoWriter_fourcc(*'mp4v'), 60.0, (SCREEN_SIZE))
print("Start recording")

while True:
    img = pyautogui.screenshot()
    numpy_frame = numpy.array(img)
    frame = cv2.cvtColor(numpy_frame, cv2.COLOR_BGR2RGB)
    vid.write(frame)

    if keyboard.is_pressed('x'):
        print("Stop recording")
        break

cv2.destroyAllWindows()
vid.release()
